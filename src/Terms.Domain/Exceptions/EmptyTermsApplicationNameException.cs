using System;
using Terms.Shared.Abstractions;
namespace Terms.Domain
{
    public class EmptyTermsApplicationNameException : Exception, TermsException
    {
        #region Constructor
        public EmptyTermsApplicationNameException() : base("Terms application name cannot be empty")
        {
        }
        #endregion
    }
}