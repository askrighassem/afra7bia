using System;
using Terms.Shared.Abstractions;
namespace Terms.Domain
{
    public class EmptyTermsContentException : Exception, TermsException
    {
        #region Constructor
        public EmptyTermsContentException() : base("Terms content cannot be empty")
        {
        }
        #endregion
    }

}