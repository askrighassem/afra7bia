using Terms.Shared.Abstractions;
namespace Terms.Domain
{
    public class EmptyTermsIdException : TermsException
    {
        #region Constructor
        public EmptyTermsIdException() : base("Terms id cannot be empty")
        {
        }
        #endregion
        
    }
    
}