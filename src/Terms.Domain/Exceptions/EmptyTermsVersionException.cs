using Terms.Shared.Abstractions;
namespace Terms.Domain
{
    public class EmptyTermsVersionException : TermsException
    {
        #region Constructor
        public EmptyTermsVersionException() : base("Terms content cannot be empty")
        {
        }
        #endregion
        
    }
    
}