using System.Threading.Tasks;

namespace Terms.Domain.Repositories
{
    public interface ITermsRepository
    {
        Task <Terms> GetAsync(TermsId id); 
        Task AddAsync(TermsId id);
        Task UpdateAsync(TermsId id);
        Task DeleteAsync(TermsId id);       
    }
}