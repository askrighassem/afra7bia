using Terms.Domain;

public record TermsContent
{
    #region Constructor
    public TermsContent(string value)
    {
        if(string.IsNullOrWhiteSpace(value))
        {
            throw  new EmptyTermsContentException();
        }
    }
    #endregion
    #region Properties
    public string Value {get; }
    #endregion
    #region Methods
    public static implicit operator string (TermsContent termsContent) => termsContent.Value;
    public static implicit operator TermsContent (string termsContent) => new(termsContent);
    #endregion 
}