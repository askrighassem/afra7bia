using System;

public record TermsVersion
{
    #region Constructor
    public TermsVersion(string value)
    {
        if(string.IsNullOrWhiteSpace(value))
        {
            throw  new NotImplementedException();
        }
    }
    #endregion
    #region Properties
    public string Value {get; }
    #endregion
    #region Methods
    public static implicit operator string (TermsVersion termsVersion) => termsVersion.Value;
    public static implicit operator TermsVersion (string termsVersion) => new(termsVersion);
    #endregion 
}