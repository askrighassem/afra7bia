using System;
using System.Linq.Expressions;
using Terms.Domain;

public record TermsApplication
{
    #region Constructor
    public TermsApplication(string value)
    {
        if(string.IsNullOrWhiteSpace(value))
        {
            throw new EmptyTermsApplicationNameException();
        }
        Value = value;
    }
    #endregion
    #region Properties
    public string Value {get; }
    #endregion
    #region Methods
    public static implicit operator string (TermsApplication termsApplicationName) => termsApplicationName.Value;
    public static implicit operator TermsApplication (string termsApplicationName) => new(termsApplicationName);
    #endregion 

}