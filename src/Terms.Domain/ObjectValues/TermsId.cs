using System;
using System.Linq.Expressions;
using Terms.Domain;

public record TermsId
{
    #region Constructor
    public TermsId(Guid value)
    {
        if(value == Guid.Empty)
        {
            throw new EmptyTermsApplicationNameException();
        }
        Value = value;
    }
    #endregion
    #region Properties
    public Guid Value {get; }
    #endregion
    #region Methods
    public static implicit operator Guid (TermsId termsId) => termsId.Value;
    public static implicit operator TermsId (Guid termsId) => new(termsId);
    #endregion 

}