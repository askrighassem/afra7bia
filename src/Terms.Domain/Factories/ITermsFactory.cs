namespace Terms.Domain
{
    public interface ITermsFactory
    {
        Terms Create(TermsApplication applicationName,  TermsVersion version,  TermsContent content); 
    }
}
