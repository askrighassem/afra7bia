namespace Terms.Domain
{
    public sealed class TermsFactory :ITermsFactory
    {
        public Terms Create(TermsApplication applicationName,  TermsVersion version,  TermsContent content)
        => new (applicationName, version, content);
    }
}
