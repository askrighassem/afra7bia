﻿using System;

namespace Terms.Domain
{
    public sealed class Terms
    {
        #region Constructor
        public  Terms (TermsApplication applicationName,  TermsVersion version,  TermsContent content)
        {
          
            _applicationName = applicationName;
            _version = version;
            _content = content; 
        }
        #endregion
        #region Properties
        public Guid Id { get; private set; }
        private string _applicationName;
        private string _version;
        private string _content;
        #endregion
        
    }
}
