using Terms.Shared.Concretes;
using Microsoft.Extensions.DependencyInjection;

namespace Terms.Shared
{
    public static class Extensions
    {
        public static IServiceCollection AddCommands(this IServiceCollection services)
        {
            services.AddSingleton<InMemoryCommandDispatcher, InMemoryCommandDispatcher>(); 
            return services; 
        }
    }
}