﻿using System;
using System.Collections;
using System.Threading.Tasks;

namespace Terms.Shared.Concretes
{
    internal sealed  class InMemoryCommandDispatcher : ICommandDispatcher
    {
        private readonly IServiceProvider _serviceProvider;
        public InMemoryCommandDispatcher(IServiceProvider serviceProvider) => _serviceProvider = serviceProvider;
        
        public async Task DispatchCommand<TCommand>(TCommand command) where TCommand : class ,ICommand
        {
            using var scope = _serviceProvider.CreateScope();
            var handler = scope.ServiceProvider.GetRequiredService<ICommandHandler<TCommand>>();
            await handler.HandleAsync(command);
            
        }
    }
}
