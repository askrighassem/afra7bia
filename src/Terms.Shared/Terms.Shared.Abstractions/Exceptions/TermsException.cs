﻿using System;

namespace Terms.Shared.Abstractions
{
    public abstract class TermsException :Exception
    {
        protected TermsException(string message) : base(message)
        {
            
        }
    }
}
