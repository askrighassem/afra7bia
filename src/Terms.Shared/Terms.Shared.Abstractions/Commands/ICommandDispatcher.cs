using System.Threading.Tasks;

namespace Terms.Shared
{
    public interface ICommandDispatcher
    {
        Task DispatchCommand<TCommand>(TCommand command) where TCommand : class, ICommand;
    }
}